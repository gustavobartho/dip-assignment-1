"""
Name: Gustavo Bartholomeu Trad Souza
USP number: 11219216
Course code: SCC0251
Year/Semester: 2020/1
Title: Assignment 1 : intensity transformations

"""

# Importing the necessary lybraries
import numpy as np
import imageio

"""
import matplotlib.pyplot as plt
def plotImgs(img1, img2, figsize = (12, 12), cmap='gray'): # Function to plot the two images
    plt.figure(figsize=figsize) 
 
    plt.subplot(121) 
    plt.imshow(img1, cmap=cmap)
    plt.axis('off') 
    
    plt.subplot(122) 
    plt.imshow(img2, cmap=cmap)
    plt.axis('off')
"""
##------------------------------------------------------------
def inversion(in_img): # Inversion 
    out_img = (255 - in_img)
    return out_img

##------------------------------------------------------------
def contrastMod(in_img, c, d): # Contrast modulation 
    a = np.amin(in_img) # Get the min pixel value of the image
    b = np.amax(in_img) # Get the max pixel value of the image
    out_img = ( (in_img - a) * ((d - c) / (b - a)) + c )# Apply the transformation
    return out_img

##------------------------------------------------------------
def logFunction(in_img): # Logarithmic Function
    out_img = (255 * ((np.log2(1 + in_img))/(np.log2(1 + np.amax(in_img))))) # Apply the transformation
    return out_img

##------------------------------------------------------------
def gammaAdj(in_img, W, lambd): # Gamma adjustment
    out_img = (W * np.power(in_img, lambd))
    return out_img

##--------------------------------------------------------------------------------------------------
# Get the file name from the user and load the image
    
filename = str(input()).rstrip()

filepath = "" # Save the file path, if the path changes, just change this variable
in_img = imageio.imread(filepath+filename)


in_img = in_img.astype(np.float) # Convert the image to float type

method = int(input())

save = int(input())

if(method == 1): # Inversion
    out_img = inversion(in_img)
    #plotImgs(in_img, out_img)
    
elif(method == 2): # Contrast modulation
    c = int(input())
    d = int(input())
    out_img = contrastMod(in_img, c, d)
    #plotImgs(in_img, out_img)
    
elif(method == 3): # Logarithmic Function
    out_img = logFunction(in_img)
    #plotImgs(in_img, out_img)
    
elif(method == 4): #  Gamma adjustment
    W = int(input())
    lambd = float(input())
    out_img = gammaAdj(in_img, W, lambd)
    #plotImgs(in_img, out_img)
    
else: # If the option doesn't exist the output image will be the input image
    out_img = in_img
    
'''
# Visit each element in the image arrays to compute the RSE
RSE = 0
for i in range(np.shape(in_img)[0]):
    for j in range(np.shape(in_img)[1]):
        RSE += ((out_img[i][j].astype(np.float) - in_img[i][j].astype(np.float))*(out_img[i][j].astype(np.float) - in_img[i][j].astype(np.float))) 
RSE = np.sqrt(RSE)
'''

#Compute the RSE using numpy functions
#   np.sqrt(num) -> Squared root of num
#   np.sum(arr) -> Sum of the elements of the array arr
#   np.power(arr, bum) -> Power each element of the array arr by the scalar num
#   np.subtract(arr1, arr2) -> subtract each element of array arr2 from the correspondent element in array arr1
RSE = np.sqrt(np.sum(np.power(np.subtract(out_img.astype(np.float), in_img.astype(np.float)), 2)))
print("%.4f"%RSE)
 
if (save == 1):
    out_img = out_img.astype(np.uint8) #Convert the transformed image to uint8 type
    imageio.imwrite(filepath+'output_img.png',out_img)

